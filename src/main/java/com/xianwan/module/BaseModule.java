package com.xianwan.module;

import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
@IocBean
public abstract class BaseModule {

    @Inject
    protected Dao dao;

    protected static final Log log = Logs.get();
}
