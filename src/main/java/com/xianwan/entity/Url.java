package com.xianwan.entity;

import org.nutz.dao.entity.annotation.*;

/**
 * @author Howe
 */
@Table("url")
public class Url extends BaseEntity {

    /**
     * 短Key
     */
    @Column("short_key")
    private String shortKey;

    public String getShortKey() {
        return shortKey;
    }

    public void setShortKey(String shortKey) {
        this.shortKey = shortKey;
    }

    /**
     * 链接内容标题
     */
    @Column("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 长网址
     */
    @Column("long_url")
    private String longUrl;

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    /**
     * 添加时间
     */
    @Column("add_time")
    private java.util.Date addTime;

    public java.util.Date getAddTime() {
        return addTime;
    }

    public void setAddTime(java.util.Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 备注
     */
    @Column("remark")
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}