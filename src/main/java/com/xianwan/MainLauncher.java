package com.xianwan;

import org.nutz.boot.NbApp;
import org.nutz.mvc.annotation.Encoding;
import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.SetupBy;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@Fail("http:500")
@Encoding(input = "utf8", output = "utf8")
@SetupBy(value = MainSetup.class)
public class MainLauncher {

    public static void main(String[] args) {
        new NbApp(MainLauncher.class).run();
    }
}
